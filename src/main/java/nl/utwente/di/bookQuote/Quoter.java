package nl.utwente.di.bookQuote;

public class Quoter {
    public double getBookPrice(String isbn) {
        double celsius = Integer.parseInt(isbn);
        return (celsius*1.8)+32;
    }
}
